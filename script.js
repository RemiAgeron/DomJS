
const li = document.getElementsByTagName('li');

const ul = document.getElementsByTagName('ul')[0];

// contenu H1 : 
console.log(document.querySelector('.title').innerHTML);

// 1ere balise li : 
console.log(li[0].innerHTML);

// derniere balise li : 
console.log(li[li.length - 1].innerHTML);

// 3eme balise li : 
console.log(li[3 - 1].innerHTML);

// parent balise ul :
console.log(ul.parentElement);

// parent 4eme balise li : 
console.log(li[4 - 1].parentElement);

// "hover" rouge balise ul :
ul.addEventListener('mouseenter', () => {
  ul.style.color = "red";
});

ul.addEventListener('mouseleave', () => {
  ul.style.color = "black";
});